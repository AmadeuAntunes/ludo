﻿using Ludo.modelos;
using Ludo.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ludo
{
    public partial class Form1 : Form
    {
        List<TcpClient> LTcpClient = new List<TcpClient>();
        TcpListener mTcpListener;
        TcpClient mTcpClient;
        IPAddress ipaddr;
        int nporto;
        private bool btnligado = false;
        byte[] mRx;
        /*********************************************************************************/
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
        /*********************************************************************************/

        public void bloquearPecas()
        {
            PlayerAzul1.Visible = false;
            PlayerAmarelo1.Visible = false;
            PlayerVermelho1.Visible = false;
            PlayerVerde1.Visible = false;
        }
        /*********************************************************************************/
        public static void SendMessage(string Msg)
        {
            MessageBox.Show(Msg);
        }
        /*********************************************************************************/
        private void desbloquearPecas()
        {

            foreach (var jogador in Jogadores.ListaJogadores)
            {
                switch (jogador.CorJogador.ToString())
                {
                    case "verde":
                        {
                            PlayerVerde1.Visible = true;
                            break;
                        }
                    case "amarelo":
                        {
                            PlayerAmarelo1.Visible = true;
                            break;
                        }
                    case "vermelho":
                        {
                            PlayerVermelho1.Visible = true;
                            break;
                        }
                    case "azul":
                        {
                            PlayerAzul1.Visible = true;
                            break;
                        }
                }
            }
        }
        /*********************************************************************************/

        private void validar_campos()
        {
            if (!btnligado)
            {
              
                if (!IPAddress.TryParse(TextBoxIP.Text, out ipaddr))
                {
                    SendMessage("Endreço IP Inválido" + Environment.NewLine);
                    return;
                }

                
                if (!int.TryParse(TextBoxPorto.Text, out nporto))
                {
                    SendMessage("Porto Inválido" + Environment.NewLine);
                    return;
                }

                if (nporto < 1024 || nporto > 65535)
                {
                    SendMessage("Porto Inválido" + Environment.NewLine);
                    return;
                }

                if (TextBoxNomeJogador.Text.Equals(String.Empty))
                {
                    SendMessage("Introduza o seu nome" + Environment.NewLine);
                    return;
                }


                btnligado = true;
                desbloquearPecas();
                TextBoxPorto.Enabled = false;
                TextBoxIP.Enabled = false;
                TextBoxNomeJogador.Enabled = false;
                ButtonLigar.Text = "Disconnectar";
                ButtonLigar.BackColor = Color.Green;
                BackgroundImage = global::Ludo.Properties.Resources.Artboard_42;
                ConectarServidor();
            }
            else
            {
                btnligado = false;
                bloquearPecas();
                TextBoxPorto.Invoke((MethodInvoker)delegate
                {
                    TextBoxPorto.Enabled = true;
                    TextBoxIP.Enabled = true;
                    TextBoxNomeJogador.Enabled = true;
                    ButtonLigar.Text = "Connectar";
                    ButtonLigar.BackColor = Color.DarkSlateGray;
                    BackgroundImage = global::Ludo.Properties.Resources.Artboard_3;
                });
                Jogadores.ListaJogadores.Clear();

            }
        }


        void ConectarServidor()
        {
            mTcpClient = new TcpClient();
            mTcpClient.BeginConnect(ipaddr, nporto, onCompleteConnect, mTcpClient);
        }


        private void onCompleteConnect(IAsyncResult iar)
        {
            TcpClient tcpc;
            tcpc = (TcpClient)iar.AsyncState;
            try
            {
                tcpc.EndConnect(iar);
            }
            catch (Exception)
            {
              
                MessageBox.Show("Ligação não estabelecida " + Environment.NewLine);
                return;
            }

           MessageBox.Show("Ligação estabelecida " + Environment.NewLine);
     
            mRx = new byte[1460];
           tcpc.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, mTcpClient);
        }

        private void OnCompleteRead(IAsyncResult iar)
        {
            TcpClient tcpc;
            string strRecv = String.Empty;
            int nReadBytes = 0;
            tcpc = (TcpClient)iar.AsyncState;
            nReadBytes = tcpc.GetStream().EndRead(iar);
            strRecv = ASCIIEncoding.ASCII.GetString(mRx, 0, nReadBytes);
            MessageBox.Show(strRecv);
            mRx = new byte[1460];
            mTcpClient.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, mTcpClient);

        }



        /*********************Construtor*******************************/
        public Form1()
        {
            InitializeComponent();
            TextBoxIP.Text = GetLocalIPAddress();
            TextBoxPorto.Text = "1500";
            BackgroundImage = global::Ludo.Properties.Resources.Artboard_3;
        }
        /*********************Eventos*******************************/
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonLigar_Click(object sender, EventArgs e)
        {
            validar_campos();
        }

    }
}
