﻿using Ludo.modelos;
using Ludo.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ludo
{
    public partial class Form1 : Form
    {
        List<TcpClient> LTcpClient = new List<TcpClient>();
        TcpListener mTcpListener;
        TcpClient mTcpClient;
        IPAddress ipaddr;
        int nporto;
        private bool btnligado = false;
        byte[] mRx;
        /********************************************************************/

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
        /********************************************************************/

        public void bloquearPecas()
        {
            PlayerAzul1.Visible = false;
            PlayerAmarelo1.Visible = false;
            PlayerVermelho1.Visible = false;
            PlayerVerde1.Visible = false;
        }
        /********************************************************************/
        public static void SendMessage(string Msg)
        {
            MessageBox.Show(Msg);
        }
        /********************************************************************/
       private void desbloquearPecas()
        {

            foreach (var jogador in Jogadores.ListaJogadores)
            {
                switch (jogador.CorJogador.ToString())
                {
                    case "verde":
                        {
                            PlayerVerde1.Visible = true;
                            break;
                        }
                    case "amarelo":
                        {
                            PlayerAmarelo1.Visible = true;
                            break;
                        }
                    case "vermelho":
                        {
                            PlayerVermelho1.Visible = true;
                            break;
                        }
                    case "azul":
                        {
                            PlayerAzul1.Visible = true;
                            break;
                        }
                }
            }
        }

        /********************************************************************/
        private void validar_campos()
        {
            if (!btnligado)
            {

                if (!IPAddress.TryParse(TextBoxIP.Text, out ipaddr))
                {
                    SendMessage("Endreço IP Inválido" + Environment.NewLine);
                    return;
                }


                if (!int.TryParse(TextBoxPorto.Text, out nporto))
                {
                    SendMessage("Porto Inválido" + Environment.NewLine);
                    return;
                }

                if (nporto < 1024 || nporto > 65535)
                {
                    SendMessage("Porto Inválido" + Environment.NewLine);
                    return;
                }

                if (TextBoxNomeJogador.Text.Equals(String.Empty))
                {
                    SendMessage("Introduza o seu nome" + Environment.NewLine);
                    return;
                }

                Jogadores.ListaJogadores.Add(new Jogador(TextBoxNomeJogador.Text));
                
                
               
                btnligado = true;
                desbloquearPecas();
                TextBoxPorto.Enabled = false;
                TextBoxIP.Enabled = false;
                TextBoxNomeJogador.Enabled = false;
                ButtonLigar.Text = "Disconnectar";
                ButtonLigar.BackColor = Color.Green;
                BackgroundImage = global::Ludo.Properties.Resources.Artboard_42;
                ButtonIniciarJogo.Visible = true;
                mTcpListener = new TcpListener(ipaddr, nporto);
                aceitarClientes();
            }
            else
            {
                btnligado = false;
                bloquearPecas();
                ButtonIniciarJogo.Visible = false;
                TextBoxPorto.Invoke((MethodInvoker)delegate
                {
                    TextBoxPorto.Enabled = true;
                    TextBoxIP.Enabled = true;
                    TextBoxNomeJogador.Enabled = true;
                    ButtonLigar.Text = "Connectar";
                    ButtonLigar.BackColor = Color.DarkSlateGray;
                    BackgroundImage = global::Ludo.Properties.Resources.Artboard_3;
                   
                });
                Jogadores.ListaJogadores.Clear();

            }
        }

        /********************************************************************/
        void aceitarClientes()
        {
            mTcpListener.Start();
            mTcpListener.BeginAcceptTcpClient(onCompletAcceptClient, mTcpListener);
        }
        /********************************************************************/
        private void onCompletAcceptClient(IAsyncResult iar)
        {
            TcpListener tcpl = (TcpListener)iar.AsyncState;
            TcpClient mTcpClient = tcpl.EndAcceptTcpClient(iar);

            if (LTcpClient.Count < 3)
            {
                LTcpClient.Add(mTcpClient);
                enviar("ok"); 



                mRx = new byte[1460];
                mTcpClient.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, mTcpClient);
                aceitarClientes();
            }
            else
            {
                MessageBox.Show("recusou a ligação : server");
                mTcpClient.GetStream().Close();
                mTcpClient.Close();
                aceitarClientes();

            }
        }

        /********************************************************************/
        private void enviar(string msg)
        {
            byte[] sendTxt = new byte[1460];
            mRx = new byte[1460];
            foreach (var mTcpClient in LTcpClient)
            {
                if (mTcpClient.Client.Connected)
                {
                    sendTxt = ASCIIEncoding.ASCII.GetBytes(msg);
                    mTcpClient.GetStream().BeginWrite(sendTxt, 0, sendTxt.Length, OnCompleteWrite, mTcpClient);
                }

            }
           
        }

        /********************************************************************/
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        /********************************************************************/

        private void ButtonLigar_Click(object sender, EventArgs e)
        {
            validar_campos();
        }
       
        /********************************************************************/
        private void ButtonIniciarJogo_Click(object sender, EventArgs e)
        {

           
            MenuCor form = new MenuCor();
            form.Show();


            /* Adiciona o inicio do jogo*/
            foreach (var mTcpClient in LTcpClient)
            {
                mRx = new byte[1460];
                mTcpClient.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, mTcpClient);
            }
        }
        /********************************************************************/
        private void OnCompleteRead(IAsyncResult iar)
        {
            TcpClient tcpc;
            string strRecv = String.Empty;
            int nReadBytes = 0;
            tcpc = (TcpClient)iar.AsyncState;
            nReadBytes = tcpc.GetStream().EndRead(iar);
            strRecv = ASCIIEncoding.ASCII.GetString(mRx, 0, nReadBytes);
            MessageBox.Show(strRecv);
            mRx = new byte[14620];
            tcpc.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, tcpc);
            
        }

        /********************************************************************/
        private void OnCompleteWrite(IAsyncResult iar)
        {
            TcpClient tcpc = (TcpClient)iar.AsyncState;
            tcpc.GetStream().EndWrite(iar);
          

        }
        /********************************************************************/
        public Form1()
        {
            InitializeComponent();
            TextBoxIP.Text = GetLocalIPAddress();
            TextBoxPorto.Text = "1500";
            BackgroundImage = global::Ludo.Properties.Resources.Artboard_3;
           
        }
        /********************************************************************/

    }
}
