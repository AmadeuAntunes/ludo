﻿namespace Ludo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ButtonFechar = new System.Windows.Forms.Button();
            this.ButtonLigar = new System.Windows.Forms.Button();
            this.ButtonIniciarJogo = new System.Windows.Forms.Button();
            this.TextBoxIP = new System.Windows.Forms.TextBox();
            this.TextBoxPorto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxNomeJogador = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PlayerVerde1 = new System.Windows.Forms.PictureBox();
            this.PlayerAmarelo1 = new System.Windows.Forms.PictureBox();
            this.PlayerAzul1 = new System.Windows.Forms.PictureBox();
            this.PlayerVermelho1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerVerde1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerAmarelo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerAzul1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerVermelho1)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonFechar
            // 
            this.ButtonFechar.BackColor = System.Drawing.Color.Maroon;
            this.ButtonFechar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.ButtonFechar.Location = new System.Drawing.Point(500, 466);
            this.ButtonFechar.Name = "ButtonFechar";
            this.ButtonFechar.Size = new System.Drawing.Size(153, 30);
            this.ButtonFechar.TabIndex = 0;
            this.ButtonFechar.Text = "Fechar";
            this.ButtonFechar.UseVisualStyleBackColor = false;
            this.ButtonFechar.Click += new System.EventHandler(this.button1_Click);
            // 
            // ButtonLigar
            // 
            this.ButtonLigar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ButtonLigar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.ButtonLigar.Location = new System.Drawing.Point(500, 430);
            this.ButtonLigar.Name = "ButtonLigar";
            this.ButtonLigar.Size = new System.Drawing.Size(153, 30);
            this.ButtonLigar.TabIndex = 1;
            this.ButtonLigar.Text = "Ligar Servidor";
            this.ButtonLigar.UseVisualStyleBackColor = false;
            this.ButtonLigar.Click += new System.EventHandler(this.ButtonLigar_Click);
            // 
            // ButtonIniciarJogo
            // 
            this.ButtonIniciarJogo.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ButtonIniciarJogo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.ButtonIniciarJogo.Location = new System.Drawing.Point(500, 303);
            this.ButtonIniciarJogo.Name = "ButtonIniciarJogo";
            this.ButtonIniciarJogo.Size = new System.Drawing.Size(153, 30);
            this.ButtonIniciarJogo.TabIndex = 2;
            this.ButtonIniciarJogo.Text = "Iniciar Jogo";
            this.ButtonIniciarJogo.UseVisualStyleBackColor = false;
            this.ButtonIniciarJogo.Visible = false;
            this.ButtonIniciarJogo.Click += new System.EventHandler(this.ButtonIniciarJogo_Click);
            // 
            // TextBoxIP
            // 
            this.TextBoxIP.Location = new System.Drawing.Point(538, 401);
            this.TextBoxIP.Name = "TextBoxIP";
            this.TextBoxIP.Size = new System.Drawing.Size(115, 20);
            this.TextBoxIP.TabIndex = 3;
            // 
            // TextBoxPorto
            // 
            this.TextBoxPorto.Location = new System.Drawing.Point(538, 378);
            this.TextBoxPorto.Name = "TextBoxPorto";
            this.TextBoxPorto.Size = new System.Drawing.Size(115, 20);
            this.TextBoxPorto.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(502, 381);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Porto";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(505, 408);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "IP";
            // 
            // TextBoxNomeJogador
            // 
            this.TextBoxNomeJogador.Location = new System.Drawing.Point(505, 352);
            this.TextBoxNomeJogador.Name = "TextBoxNomeJogador";
            this.TextBoxNomeJogador.Size = new System.Drawing.Size(148, 20);
            this.TextBoxNomeJogador.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(505, 336);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Nome do Jogador";
            // 
            // PlayerVerde1
            // 
            this.PlayerVerde1.BackColor = System.Drawing.Color.Transparent;
            this.PlayerVerde1.Image = ((System.Drawing.Image)(resources.GetObject("PlayerVerde1.Image")));
            this.PlayerVerde1.Location = new System.Drawing.Point(51, 336);
            this.PlayerVerde1.Name = "PlayerVerde1";
            this.PlayerVerde1.Size = new System.Drawing.Size(35, 36);
            this.PlayerVerde1.TabIndex = 9;
            this.PlayerVerde1.TabStop = false;
            this.PlayerVerde1.Visible = false;
            // 
            // PlayerAmarelo1
            // 
            this.PlayerAmarelo1.BackColor = System.Drawing.Color.Transparent;
            this.PlayerAmarelo1.Image = ((System.Drawing.Image)(resources.GetObject("PlayerAmarelo1.Image")));
            this.PlayerAmarelo1.Location = new System.Drawing.Point(348, 336);
            this.PlayerAmarelo1.Name = "PlayerAmarelo1";
            this.PlayerAmarelo1.Size = new System.Drawing.Size(35, 36);
            this.PlayerAmarelo1.TabIndex = 10;
            this.PlayerAmarelo1.TabStop = false;
            this.PlayerAmarelo1.Visible = false;
            // 
            // PlayerAzul1
            // 
            this.PlayerAzul1.BackColor = System.Drawing.Color.Transparent;
            this.PlayerAzul1.Image = ((System.Drawing.Image)(resources.GetObject("PlayerAzul1.Image")));
            this.PlayerAzul1.Location = new System.Drawing.Point(337, 131);
            this.PlayerAzul1.Name = "PlayerAzul1";
            this.PlayerAzul1.Size = new System.Drawing.Size(35, 36);
            this.PlayerAzul1.TabIndex = 11;
            this.PlayerAzul1.TabStop = false;
            this.PlayerAzul1.Visible = false;
            // 
            // PlayerVermelho1
            // 
            this.PlayerVermelho1.BackColor = System.Drawing.Color.Transparent;
            this.PlayerVermelho1.Image = ((System.Drawing.Image)(resources.GetObject("PlayerVermelho1.Image")));
            this.PlayerVermelho1.Location = new System.Drawing.Point(132, 131);
            this.PlayerVermelho1.Name = "PlayerVermelho1";
            this.PlayerVermelho1.Size = new System.Drawing.Size(35, 36);
            this.PlayerVermelho1.TabIndex = 12;
            this.PlayerVermelho1.TabStop = false;
            this.PlayerVermelho1.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(534, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 24);
            this.label4.TabIndex = 13;
            this.label4.Text = "Servidor";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Ludo.Properties.Resources.Artboard_42;
            this.ClientSize = new System.Drawing.Size(655, 502);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PlayerVermelho1);
            this.Controls.Add(this.PlayerAzul1);
            this.Controls.Add(this.PlayerAmarelo1);
            this.Controls.Add(this.PlayerVerde1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBoxNomeJogador);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxPorto);
            this.Controls.Add(this.TextBoxIP);
            this.Controls.Add(this.ButtonIniciarJogo);
            this.Controls.Add(this.ButtonLigar);
            this.Controls.Add(this.ButtonFechar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PlayerVerde1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerAmarelo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerAzul1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerVermelho1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonFechar;
        private System.Windows.Forms.Button ButtonLigar;
        private System.Windows.Forms.Button ButtonIniciarJogo;
        private System.Windows.Forms.TextBox TextBoxIP;
        private System.Windows.Forms.TextBox TextBoxPorto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxNomeJogador;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox PlayerVerde1;
        private System.Windows.Forms.PictureBox PlayerAmarelo1;
        private System.Windows.Forms.PictureBox PlayerAzul1;
        private System.Windows.Forms.PictureBox PlayerVermelho1;
        private System.Windows.Forms.Label label4;
    }
}

