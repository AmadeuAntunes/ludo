﻿using Ludo.modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ludo.View
{
    public partial class MenuCor : Form
    {
        public MenuCor()
        {
            InitializeComponent();
            foreach (var jogador in Jogadores.ListaJogadores)
            {
                switch (jogador.CorJogador.ToString())
                {
                    case "verde":
                        {
                            pictureBox1.Visible = true;
                            break;
                        }
                    case "amarelo":
                        {
                            pictureBox3.Visible = true;
                            break;
                        }
                    case "vermelho":
                        {
                            pictureBox2.Visible = true;
                            break;
                        }
                    case "azul":
                        {
                            pictureBox4.Visible = true;
                            break;
                        }
                }


            }
        }

        private void ButtonFechar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
